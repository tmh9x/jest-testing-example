const fetchData = require('./fetchData');
const fetch = require('node-fetch');

jest.mock('node-fetch', () => jest.fn());

test('fetches successfully data from JSONPlaceholder API', async () => {
    const mockResponse = { userId: 1, id: 1, title: 'mock title', body: 'mock body' };
    fetch.mockResolvedValueOnce({
        ok: true,
        json: async () => mockResponse,
    });

    const data = await fetchData('https://jsonplaceholder.typicode.com/posts/1');
    expect(data).toEqual(mockResponse);
    expect(fetch).toHaveBeenCalledWith('https://jsonplaceholder.typicode.com/posts/1');
});

test('fetch fails with an error', async () => {
    fetch.mockResolvedValueOnce({
        ok: false,
    });

    await expect(fetchData('https://jsonplaceholder.typicode.com/posts/1')).rejects.toThrow('Network response was not ok');
    expect(fetch).toHaveBeenCalledWith('https://jsonplaceholder.typicode.com/posts/1');
});
